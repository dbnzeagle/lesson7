package com.test.lesson_7

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.Icon
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat

class CustomReceiver : BroadcastReceiver() {
    companion object {
        const val CUSTOM_ACTION = "com.test.lesson_7.CUSTOM_ACTION"
        const val NOTIFICATION_ACTION = "com.test.lesson_7.NOTIFICATION_ACTION"
    }

    override fun onReceive(context: Context, intent: Intent?) {
        if (intent?.action == CUSTOM_ACTION) {
            writeToDB(context)
        } else {
            Log.i("Notification", "Уведомление")
        }
    }

    private fun writeToDB(context: Context) {
        val user = User(id = 999, phone = "123456789", gender = "male", fio = "I I I", age = "20")
        LocalDB(context).addUser(user)
        createNotification(context)
    }

    private fun createNotification(context: Context) {
        var channel: NotificationChannel? = null
        val intent = Intent(context, CustomReceiver::class.java)
        intent.action = NOTIFICATION_ACTION
        val pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channel = createChannel()
            val notification =
                Notification.Builder(context, channel?.id)
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setContentTitle("Уведомление")
                    .setContentText("Записал в БД")
                    .setLargeIcon(Icon.createWithResource(context, R.mipmap.ic_launcher_round))
                    .addAction(R.drawable.ic_launcher_background, "Action", pendingIntent)
                    .build()

            App.notificationManager.createNotificationChannel(channel!!)
            App.notificationManager.notify(99, notification)
        } else {
            val notification = NotificationCompat.Builder(context, "CUSTOM_CHANNEL")
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle("Уведомление")
                .setContentText("Записал в БД")
                .addAction(R.drawable.ic_launcher_background, "Action", pendingIntent)
                .build()
            App.notificationManager.notify(5, notification)
        }
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private fun createChannel(): NotificationChannel? {
        val channel =
            NotificationChannel(
                "CUSTOM_CHANNEL",
                "Запись в базу данных",
                NotificationManager.IMPORTANCE_HIGH
            )

        channel.enableLights(true)
        channel.lightColor = Color.RED
        channel.enableVibration(true)
        channel.vibrationPattern = longArrayOf(400, 100, 200, 300, 100)
        return channel
    }
}