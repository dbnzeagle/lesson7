package com.test.lesson_7

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import java.util.concurrent.TimeUnit

class CustomWorker(private val context: Context, private val params: WorkerParameters) :
    Worker(context, params) {


    override fun doWork(): Result {
        val result: Result?
        result = try {
            Log.i("Try","Еще не сплю.")
            TimeUnit.SECONDS.sleep(1L)
            throw Throwable()
            Log.i("Try","Уже поспал.")
            Result.success()
        } catch (e: Throwable) {
            Log.i("Throwable","Упал.")
            Result.failure()
        }

        return result
    }

}