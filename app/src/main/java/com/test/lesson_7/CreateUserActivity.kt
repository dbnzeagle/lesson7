package com.test.lesson_7

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_create_user.*

class CreateUserActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_user)
        initView()
    }

    private fun initView() {
        button.setOnClickListener {
            val fio = create_user_fio.text.toString()
            val age = create_user_age.text.toString()
            val gender = create_user_gender.text.toString()
            val phone = create_user_phone.text.toString()
            val id = getValidId() + 1
            val user = User(id = id, age = age, fio = fio, gender = gender, phone = phone)
            saveToDB(user)
        }
    }

    private fun getValidId(): Int {
        val localDataBase = LocalDB(applicationContext)
        return localDataBase.getUsers().size
    }

    private fun saveToDB(user: User) {
        val localDataBase = LocalDB(applicationContext)
        try {
            if (localDataBase.getUserById(user.id) == null) {
                localDataBase.addUser(user)
            } else {
                localDataBase.updateUser(user.id, user)
            }
        }catch (e:Throwable){
            localDataBase.addUser(user)
        }
    }

}