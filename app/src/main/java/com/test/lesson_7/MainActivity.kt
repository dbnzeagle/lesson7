package com.test.lesson_7

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.work.Constraints
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.test.lesson6.CustomAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
        initWorker()
        registrateAlarmManager()
    }

    override fun onResume() {
        super.onResume()
        initView()
    }

    private fun initView() {
        val localDB = LocalDB(applicationContext)
        val users = localDB.getUsers()
        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.adapter = CustomAdapter(users) { id ->
            val intent = Intent(this, UserDetailActivity::class.java)
            intent.putExtra("ID", id)
            startActivity(intent)
        }
        button2.setOnClickListener {
            val intent = Intent(this, CreateUserActivity::class.java)
            startActivity(intent)
        }
    }

    private fun initWorker(){
        val constraints = Constraints.Builder().setRequiresBatteryNotLow(true).build()

        val customWorker = OneTimeWorkRequest.Builder(CustomWorker::class.java)
            .setConstraints(constraints)
            .build()

        WorkManager.getInstance(applicationContext).enqueue(customWorker)
    }


    private fun registrateAlarmManager() {
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(this, CustomReceiver::class.java)
        intent.action = CustomReceiver.CUSTOM_ACTION
        val pendingIntent =
            PendingIntent
                .getBroadcast(
                    this,
                    0,
                    intent,
                    0
                )
        button3.setOnClickListener {
            val delaySec = 3000L
            val alarmTime = System.currentTimeMillis() + delaySec
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTime, pendingIntent)
        }
    }
}