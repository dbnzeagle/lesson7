package com.test.lesson_7

interface ILocalDBInterface {
    fun addUser(user: User) : Long?
    fun updateUser(id:Int,user: User) : User
    fun getUserById(id: Int) : User?
    fun getUsers() : List<User>
    fun deleteUserById(id: Int) : Boolean
    fun deleteAll() : Boolean
}