package com.test.lesson_7

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.test.lesson_7.LocalDB
import com.test.lesson_7.R
import kotlinx.android.synthetic.main.activity_user_detail.*

class UserDetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_detail)
        initView()
    }

    private fun initView() {
        var id = this.intent.extras?.getInt("ID") ?: 1
        val localDB = LocalDB(applicationContext)
        val user = localDB.getUserById(id)
        fio.text = user?.fio
        age.text = user?.age
        gender.text = user?.gender
        phone.text = user?.phone
    }
}