package com.test.lesson_7

import android.app.Application
import android.app.NotificationManager
import android.content.Context

class App : Application() {

    companion object {
        lateinit var notificationManager: NotificationManager
        lateinit var localDB: LocalDB
        lateinit var context: Context

        fun initContext(ctx: Context) {
            context = ctx
        }

        fun initDB(localData: LocalDB) {
            localDB = localData
        }

        fun initManager(notifManager: NotificationManager) {
            notificationManager = notifManager
        }
    }

    override fun onCreate() {
        super.onCreate()
        initContext(applicationContext)
        initDB(LocalDB(applicationContext))
        initManager(getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
    }

}