package com.test.lesson6

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.test.lesson_7.R
import com.test.lesson_7.User

class CustomAdapter(private val list: List<User>, private val onClick: (Int) -> Unit) :
    RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_user, parent, false
        )
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.id.text = list[position].id.toString()
        holder.fio.text = list[position].fio
        holder.gender.text = list[position].gender
        holder.view.setOnClickListener {
            onClick.invoke(list[position].id)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val view = view.findViewById<ConstraintLayout>(R.id.item)
        val id = view.findViewById<TextView>(R.id.textView)
        val fio = view.findViewById<TextView>(R.id.textView2)
        val gender = view.findViewById<TextView>(R.id.textView3)
    }
}