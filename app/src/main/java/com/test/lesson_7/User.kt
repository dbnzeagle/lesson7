package com.test.lesson_7

data class User(
    val id: Int,
    val fio: String,
    val gender: String,
    val phone: String,
    val age: String
)