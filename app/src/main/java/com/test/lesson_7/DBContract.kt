package com.test.lesson_7

import android.provider.BaseColumns

object DBContract {
    object UserEntity : BaseColumns {
        const val TABLE = "user"
        const val FIO = "fio"
        const val PHONE = "phone"
        const val AGE = "age"
        const val GENDER = "gender"
    }

    const val CREATE_TABLE = "CREATE TABLE ${UserEntity.TABLE} (" +
            "${BaseColumns._ID} INTEGER PRIMARY KEY," +
            "${UserEntity.FIO} TEXT," +
            "${UserEntity.PHONE} TEXT," +
            "${UserEntity.AGE} TEXT," +
            "${UserEntity.GENDER} TEXT)"

    const val DROP_TABLE = "DROP TABLE IF EXISTS ${UserEntity.TABLE}"
}