package com.test.lesson_7

import android.content.ContentValues
import android.content.Context
import android.provider.BaseColumns


class LocalDB(context: Context) : ILocalDBInterface {
    private val dbHelper = DBHelper(context = context)


    override fun addUser(user: User): Long? {
        val db = dbHelper.writableDatabase

        val values = ContentValues().apply {
            put(DBContract.UserEntity.FIO, user.fio)
            put(DBContract.UserEntity.AGE, user.age)
            put(DBContract.UserEntity.GENDER, user.gender)
            put(DBContract.UserEntity.PHONE, user.phone)
        }

        val newRowId =
            db.insert(DBContract.UserEntity.TABLE, null, values)

        db?.close()
        return newRowId
    }

    override fun updateUser(id: Int, user: User): User {
        val db = dbHelper.writableDatabase

        val values = ContentValues().apply {
            put(DBContract.UserEntity.FIO, user.fio)
            put(DBContract.UserEntity.AGE, user.age)
            put(DBContract.UserEntity.GENDER, user.gender)
            put(DBContract.UserEntity.PHONE, user.phone)
        }

        val where = "${BaseColumns._ID} = ?"
        val whereArgs = arrayOf(user.id.toString())
        val updateRow =
            db.update(DBContract.UserEntity.TABLE, values, where, whereArgs)

        return user
    }

    override fun getUserById(id: Int): User? {
        val db = dbHelper.readableDatabase

        val selection = "${BaseColumns._ID} = ?"
        val selectionArg = arrayOf(id.toString())

        val cursor =
            db.query(
                DBContract.UserEntity.TABLE,
                null,
                selection,
                selectionArg,
                null,
                null,
                null
            )

        cursor.moveToFirst()

        val userId = cursor.getInt(cursor.getColumnIndex(BaseColumns._ID))
        val fio = cursor.getString(cursor.getColumnIndex(DBContract.UserEntity.FIO))
        val age = cursor.getString(cursor.getColumnIndex(DBContract.UserEntity.AGE))
        val gender = cursor.getString(cursor.getColumnIndex(DBContract.UserEntity.GENDER))
        val phone = cursor.getString(cursor.getColumnIndex(DBContract.UserEntity.PHONE))

        val user = User(userId, fio, gender, phone, age)
        cursor.close()
        db.close()
        return user
    }

    override fun getUsers(): List<User> {
        val db = dbHelper.readableDatabase

        val usersList = arrayListOf<User>()

        val cursor =
            db.query(
                DBContract.UserEntity.TABLE,
                null,
                null,
                null,
                null,
                null,
                null
            )

        cursor.moveToFirst()

        while (!cursor.isAfterLast) {

            usersList.add(
                User(
                    cursor.getInt(cursor.getColumnIndex(BaseColumns._ID)),
                    cursor.getString(cursor.getColumnIndex(DBContract.UserEntity.FIO)),
                    cursor.getString(cursor.getColumnIndex(DBContract.UserEntity.AGE)),
                    cursor.getString(cursor.getColumnIndex(DBContract.UserEntity.GENDER)),
                    cursor.getString(cursor.getColumnIndex(DBContract.UserEntity.PHONE))
                )
            )

            cursor.moveToNext()
        }


        cursor.close()
        db.close()
        return usersList

    }

    override fun deleteUserById(id: Int): Boolean {
        val db = dbHelper.readableDatabase

        val selection = "${BaseColumns._ID} = ?"
        val selectionArg = arrayOf(id.toString())

        val deleteRows = db.delete(DBContract.UserEntity.TABLE, selection, selectionArg)
        db.close()
        return true
    }

    override fun deleteAll(): Boolean {
        val db = dbHelper.writableDatabase

        val deleteRows = db.delete(DBContract.UserEntity.TABLE, null, null)
        db.close()
        return true
    }
}