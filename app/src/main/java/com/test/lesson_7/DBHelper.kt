package com.test.lesson_7

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBHelper(context: Context) : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {
    companion object {
        const val DB_VERSION = 1
        const val DB_NAME = "lesson6.db"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(DBContract.CREATE_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL(DBContract.DROP_TABLE)
        onCreate(db)
    }
}